'use strict';

// import Vue from 'vue';
import axios from 'axios';
import { ipcRenderer } from 'electron';

var serverAddr = ipcRenderer.sendSync('get-config', 'server');
// console.log(serverAddr);

axios.defaults.baseURL = `${serverAddr}/api`;
axios.defaults.credentials = true;

// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

// const config = {
//   // baseURL: process.env.baseURL || process.env.apiUrl || ""
//   // timeout: 60 * 1000, // Timeout
//   withCredentials: true // Check cross-site Access-Control
// };

// const _axios = axios.create(config);

// axios.interceptors.request.use(
//   function (config) {
//     var apiToken = ipcRenderer.sendSync('get-apikey');
//     config.headers.Authorization = apiToken ? `Bearer ${apiToken}` : '';
//     // console.log(apiToken);
//     // console.log(config);
//     // Do something before request is sent
//     return config;
//   },
//   function (error) {
//     // Do something with request error
//     return Promise.reject(error);
//   }
// );

// // Add a response interceptor
// axios.interceptors.response.use(
//   function (response) {
//     return response;
//   },
//   function (error) {
//     // Do something with response error
//     // ipcRenderer.sendSync('logout');
//     return Promise.reject(error);
//   }
// );

// Plugin.install = function(Vue, options) {
//   Vue.axios = _axios;
//   window.axios = _axios;
//   Object.defineProperties(Vue.prototype, {
//     axios: {
//       get() {
//         return _axios;
//       }
//     },
//     $axios: {
//       get() {
//         return _axios;
//       }
//     }
//   });
// };

// Vue.use(Plugin);

export default axios;
