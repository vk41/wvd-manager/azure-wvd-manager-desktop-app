import { ipcMain } from 'electron';
const Store = require('electron-store');
const os = require('os');

const schema = {
    server: {
        type: 'string',
        format: 'uri',
        default: 'http://aqua-docker03:8081'
    },
    company: {
        type: 'string',
        default: 'Aqua Systems'
    },
    wsaddr: {
        type: 'string',
        default: 'aqua-docker03'
    },
    wsport: {
        type: 'integer',
        default: 8081
    },
    pusherkey: {
        type: 'string',
        default: 'jbgx2SorpBx1ha4nWOMtoHMiExhwcANO'
    }
};



const store = new Store({ schema });

ipcMain.on('get-config', (event, key) => {
    if (key !== undefined) {
        event.returnValue = store.get(key);
    } else {
        event.returnValue = store.store;
    }

});

export default store;

