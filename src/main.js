import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import '@babel/polyfill';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
// import '@mdi/font/css/materialdesignicons.css';
// import '@fortawesome/fontawesome-free/css/all.css';
import Echo from 'laravel-echo';
import VuetifyDialog from 'vuetify-dialog';
import 'vuetify-dialog/dist/vuetify-dialog.css';
import { ipcRenderer } from 'electron';
import axios from 'axios';
window.Pusher = require('pusher-js');

Vue.use(VuetifyDialog, {
  context: {
    vuetify
  }
});
Vue.config.productionTip = false;
// Vue.prototype.$echo = {};
window.Echo = {};

var serverAddr = ipcRenderer.sendSync('get-config', 'server');
axios.defaults.baseURL = `${serverAddr}/api`;
axios.defaults.credentials = true;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.post['Content-Type'] = 'application/json';

new Vue({
  created() {
    this.setupCommunication();
  },
  methods: {
    setupCommunication() {
      var appConfig = ipcRenderer.sendSync('get-config');
      window.Echo = new Echo({
        broadcaster: 'pusher',
        key: appConfig.pusherkey,
        wsHost: appConfig.wsaddr,
        wsPort: appConfig.wsport,
        wsPath: '/ws',
        forceTLS: false,
        disableStats: false
      });

      // axios.defaults.baseURL = `${appConfig.serverAddr}/api`;
      // axios.defaults.credentials = true;
    }
  },
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app');
