/* global __static */
'use strict';
import { app, protocol, BrowserWindow, ipcMain, Tray, Menu } from 'electron';
const path = require("path");
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib';
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer';
import './electron-services/store';

import os from 'os';
console.log(__static);
const isDevelopment = process.env.NODE_ENV !== 'production';
var serviceName = 'WVD Shutdown Notifier';

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;
let tray;

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
]);

app.setLoginItemSettings({
  openAtLogin: true,
  path: app.getPath("exe")
});

app.requestSingleInstanceLock()

async function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    height: 400,
    title: serviceName,
    // titleBarStyle: 'hiddenInset',
    frame: false,
    // frame: isDevelopment,
    webPreferences: {
      // Use pluginOptions.nodeIntegration, leave this alone
      // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
      nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION
    },
    icon: path.join(__static, 'icons', 'icon.ico')
  });
  win.setAutoHideMenuBar(true);
  // await auth.setupAuth();
  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL);
    if (!process.env.IS_TEST) win.webContents.openDevTools();
  } else {
    createProtocol('app');
    // Load the index.html when not in development
    win.loadURL('app://./index.html');
  }

  win.on('closed', () => {
    win = null;
  });

  win.on('close', function (event) {
    if (!app.isQuiting) {
      event.preventDefault();
      win.hide();
    }

    return false;
  });
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      // await installVueDevtools();
      installExtension(VUEJS_DEVTOOLS)
        .then((name) => console.log(`Added Extension:  ${name}`))
        .catch((err) => console.log('An error occurred: ', err));
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString());
    }
  }
  createWindow();
  tray = createTray();
});

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        app.quit();
      }
    });
  } else {
    process.on('SIGTERM', () => {
      app.quit();
    });
  }
}

ipcMain.on('get-devicename', (event) => {
  event.returnValue = os.hostname();
});

ipcMain.on('get-username', (event) => {
  event.returnValue = os.userInfo().username;
});

ipcMain.on('show-window', (event, data) => {
  console.log('show window called');
  console.log(data);
  win.show();
  event.returnValue = true;
});

ipcMain.on('hide-window', (event) => {
  win.hide();
  event.returnValue = true;
});


function createTray() {
  let appIcon = new Tray(path.join(__static, "power-button.png"));
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Show', click: function () {
        win.show();
      }
    },
    {
      label: 'Reset to default', click: function () {
        win.show();
        win.webContents.send('reset-trigger');
      }
    },
    {
      label: 'Reload', click: function () {
        win.show();
        win.reload();
        // win.webContents.send('reset-trigger');
      }
    }
  ]);

  appIcon.on('double-click', function (event) {
    win.show();
  });
  appIcon.setToolTip(serviceName);
  appIcon.setContextMenu(contextMenu);
  return appIcon;
}