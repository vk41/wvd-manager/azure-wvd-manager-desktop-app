
# Azure WVD Manager (auto deallocate/start) Desktop app

## Features
- Connects to Laravel backend over websocket.
- Automatically register the WVD name with backend.
- App starts on windows, minimized.
## Todo
- Add security.
- UI for user self service to change timings.
- Impliment socialize
- Make mobile app to start/stop/restart WVD (PWA or corodva).

## Setup

Configure everything in src\store\index.js file.
```
# run to buld
npm run electron:build
```
